# covidburden-master-v1

repository including the data provided by Cauchemez et al to ensure reproducibility of the computations in the article "Estimating the Burden os Sars-CoV-2 in France", published online in Science, 11 May 2020. 